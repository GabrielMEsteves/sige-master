from django.test import TestCase
from .models import Campus

class CampusTestCase(TestCase):
    def test_NV_True(self):
        campus = Campus(name='Nome do Campus', acronym='ABC', contract_type='Verde', off_peak_demand=10.0, peak_demand=20.0)
        self.assertEqual(str(campus), 'Nome do Campus')

    def test_NV_False(self):
        # Testar quando o nome está vazio
        campus = Campus(name='', acronym='ABC', contract_type='Verde', off_peak_demand=10.0, peak_demand=20.0)
     
    def test_AV_True(self):
        campus = Campus(name='Nome do Campus', acronym='ABC', contract_type='Verde', off_peak_demand=10.0, peak_demand=20.0)
        self.assertEqual(str(campus), 'Nome do Campus')

    def test_AV_False(self):
        # Testar quando o acrônimo está vazio
        campus = Campus(name='Nome do Campus', acronym='', contract_type='Verde', off_peak_demand=10.0, peak_demand=20.0)
     

    def test_TCV_True(self):
        campus = Campus(name='Nome do Campus', acronym='ABC', contract_type='Verde', off_peak_demand=10.0, peak_demand=20.0)
        self.assertEqual(str(campus), 'Nome do Campus')

    def test_TCV_False(self):
        # Testar quando o tipo de contrato é inválido
        campus = Campus(name='Nome do Campus', acronym='ABC', contract_type='Invalido', off_peak_demand=10.0, peak_demand=20.0)
        

    def test_DFPV_True(self):
        campus = Campus(name='Nome do Campus', acronym='ABC', contract_type='Verde', off_peak_demand=10.0, peak_demand=20.0)
        self.assertEqual(str(campus), 'Nome do Campus')

    def test_DFPV_False(self):
        # Testar quando a demanda fora do pico é inválida
        campus = Campus(name='Nome do Campus', acronym='ABC', contract_type='Verde', off_peak_demand=None, peak_demand=20.0)
    

    def test_DPV_True(self):
        campus = Campus(name='Nome do Campus', acronym='ABC', contract_type='Verde', off_peak_demand=10.0, peak_demand=20.0)
        self.assertEqual(str(campus), 'Nome do Campus')

    def test_DPV_False(self):
        # Testar quando a demanda de pico é inválida
        campus = Campus(name='Nome do Campus', acronym='ABC', contract_type='Verde', off_peak_demand=10.0, peak_demand=None)
        
